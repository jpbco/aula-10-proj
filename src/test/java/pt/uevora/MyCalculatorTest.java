package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5D, (Object)result);
    }

    @Test
    public void testSumofFloats() throws Exception {
        Double result = calculator.execute("2.3+3.1");

        assertEquals("The sum result of 2.3 + 3.1 must be 5.4",  5.4, (Object)result);
    }

    @Test
    public void testSumofNegatives() throws Exception {
        Double result = calculator.execute("-6.9+-0.1");

        assertEquals("The sum result of -6.9+-0.1 must be -7.0",  -7.0, (Object)result);
    }

    @Test
    public void testSumofTypes() throws Exception {
        Double result = calculator.execute("-6.9+1");

        assertEquals("The sum result of -6.9+-0.1 must be -5.9",  -5.9, (Object)result);
    }

    @Test
    public void testSumZero() throws Exception {
        Double result = calculator.execute("-6.9+0");

        assertEquals("The sum result of -6.9+0 must be -6.9",  -6.9, (Object)result);
    }

}